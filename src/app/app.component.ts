import { Component } from '@angular/core';
import { HeaderComponent } from './header/header.component';

@Component({
  selector: 'my-app',
  directives: [HeaderComponent],
  template: require('./app.component.jade'),
  styles: [ require('../styles.scss') ]
})
export class AppComponent { }
