var webpack = require("webpack");
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    polyfills: "./src/polyfills",
    vendor: "./src/vendor",
    app: "./src/main"
  },
  output: {
    path: __dirname,
    filename: "./dist/[name].bundle.js"
  },
  resolve: {
    extensions: ['', '.js', '.ts']
  },
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /\.ts/,
        loaders: ['ts-loader'],
        exclude: /node_modules/
      },
      {
        test: /\.jade/,
        loaders: ['raw', 'jade-html'],
        exclude: /node_modules/
      },
      {
        test: /\.scss/,
        loaders: ['raw', 'sass'],
        exclude: /node_modules/
      }
    ],
    noParse: [ /zone\.js\/dist\/.+/, /angular2\/bundles\/.+/ ]
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin(
      {
        name: "polyfills",
        filename: "./dist/polyfills.bundle.js"
      },
      {
        name: "vendor",
        filename: "./dist/vendor.bundle.js"
      }
    ),
    new HtmlWebpackPlugin({
      template: './src/index.jade',
      filename: 'index.html',
      minify: false
    })
  ]
};
